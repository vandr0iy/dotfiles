# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block; everything else may go below.
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi

source ~/.shrc

### exporting here only the env vars that will be used in the interactive shells
export AWS_PROFILE=vandr0iy-skuld
export AWS_DEFAULT_REGION=eu-west-1
export AWS_REGION=eu-west-1

### ZSH PONY TRICKS ###
setopt autocd
setopt append_history share_history histignorealldups

### HISTORY HACKERY ###
bindkey -v
bindkey '^R' history-incremental-search-backward
KEYTIMEOUT=1
SHOW_AWS_PROMPT=false

export HISTFILE="$HOME/.zsh_history"
export HISTSIZE=10000000
export SAVEHIST=10000000

#setopt BANG_HIST                 # Treat the '!' character specially during expansion.
setopt EXTENDED_HISTORY          # Write the history file in the ":start:elapsed;command" format.
setopt INC_APPEND_HISTORY        # Write to the history file immediately, not when the shell exits.
setopt SHARE_HISTORY             # Share history between all sessions.
setopt HIST_EXPIRE_DUPS_FIRST    # Expire duplicate entries first when trimming history.
setopt HIST_IGNORE_DUPS          # Don't record an entry that was just recorded again.
setopt HIST_IGNORE_ALL_DUPS      # Delete old recorded entry if new entry is a duplicate.
setopt HIST_FIND_NO_DUPS         # Do not display a line previously found.
setopt HIST_IGNORE_SPACE         # Don't record an entry starting with a space.
setopt HIST_SAVE_NO_DUPS         # Don't write duplicate entries in the history file.
setopt HIST_REDUCE_BLANKS        # Remove superfluous blanks before recording entry.
setopt HIST_VERIFY               # Don't execute immediately upon history expansion.
setopt HIST_BEEP                 # Beep when accessing nonexistent history.

# vi-mode is fucked up, this fixes it
vi-search-fix() {
  zle vi-cmd-mode
  zle .vi-history-search-backward
}
autoload vi-search-fix
zle -N vi-search-fix
bindkey -M viins '\e/' vi-search-fix
bindkey "^?" backward-delete-char
bindkey "^W" backward-kill-word
bindkey "^H" backward-delete-char      # Control-h also deletes the previous char
bindkey "^U" backward-kill-line

autoload -Uz add-zsh-hook
autoload -Uz vcs_info
autoload -Uz compinit
if [[ -n ${ZDOTDIR}/.zcompdump(#qN.mh+24) ]]; then
  compinit;
else
  compinit -C;
fi;
# load the oh-my-zsh plugins
# source ~/.zsh.d/plugins.sh



zstyle ':completion:*' completer _expand _complete _ignored _approximate
zstyle ':completion:*' completions 1
zstyle ':completion:*' glob 0
zstyle ':completion:*' insert-unambiguous true
zstyle ':completion:*' list-colors ''
zstyle ':completion:*' matcher-list '' 'm:{[:lower:]}={[:upper:]} m:{[:lower:][:upper:]}={[:upper:][:lower:]}' 'm:{[:lower:][:upper:]}={[:upper:][:lower:]}'
zstyle ':completion:*' max-errors 2
zstyle ':completion:*' menu select=1
zstyle ':completion:*' select-prompt %SScrolling active: current selection at %p%s
zstyle ':completion:*' substitute 1
zstyle ':completion:*' verbose true
zstyle ':completion:*:default' list-colors ${(s.:.)LS_COLORS}
zstyle ':vcs_info:*' check-for-changes false
zstyle ':vcs_info:*' enable git
zstyle ':vcs_info:git*' actionformats '%b (%a)'
zstyle ':vcs_info:git*' formats '%b'
zstyle :compinstall filename '/home/vandr0iy/.zshrc'

autoload -U +X bashcompinit && bashcompinit

# eliminates duplicates in *paths
typeset -gU cdpath fpath path

### ZINIT INSTALLER ###
### Added by Zinit's installer
if [[ ! -f $HOME/.zinit/bin/zinit.zsh ]]; then
    print -P "%F{33}▓▒░ %F{220}Installing %F{33}DHARMA%F{220} Initiative Plugin Manager (%F{33}zdharma/zinit%F{220})…%f"
    command mkdir -p "$HOME/.zinit" && command chmod g-rwX "$HOME/.zinit"
    command git clone https://github.com/zdharma/zinit "$HOME/.zinit/bin" && \
        print -P "%F{33}▓▒░ %F{34}Installation successful.%f%b" || \
        print -P "%F{160}▓▒░ The clone has failed.%f%b"
fi

source "$HOME/.zinit/bin/zinit.zsh"
autoload -Uz _zinit
(( ${+_comps} )) && _comps[zinit]=_zinit

# Load a few important annexes, without Turbo
# (this is currently required for annexes)
zinit light-mode for \
    zinit-zsh/z-a-rust \
    zinit-zsh/z-a-as-monitor \
    zinit-zsh/z-a-patch-dl \
    zinit-zsh/z-a-bin-gem-node

### End of Zinit's installer chunk

### ZINIT PLUGINS ###
load=light

#  # nocompletions lucid blockf \
#zinit ice from"gh" pick"/dev/null" \
#        multisrc"${_OMZ_SOURCES}" compile"(${(j.|.)_OMZ_SOURCES})" \
#        atinit"zpcompinit; zpcdreplay; compinit" wait"1c" #atload"_OMZ_SETTING" wait"1c"
##zinit ice from"gh" pick"*.plugin.zsh" blockf \
##      multisrc"${_OMZ_SOURCES}" compile"(${(j.|.)_OMZ_SOURCES})"
#zinit $load robbyrussell/oh-my-zsh

zinit wait lucid for \
      OMZL::git.zsh \
      OMZL::compfix.zsh \
      OMZL::directories.zsh \
      OMZL::functions.zsh \
      OMZL::termsupport.zsh \
  atload"unalias grv" \
      OMZP::git

zinit wait lucid for \
  atinit"zicompinit; zicdreplay"  \
      zdharma/fast-syntax-highlighting \
      OMZP::colored-man-pages \
      OMZP::docker-compose \
      OMZP::aws \
      OMZP::fzf \
      OMZP::jsontools \
      OMZP::urltools \
  as"completion" \
      OMZP::docker/_docker \
      OMZP::lein/_lein

# aws autocompletion
complete -C $(command -v aws_completer) aws

# prompt
zinit ice depth=1; zinit $load romkatv/powerlevel10k

# zsh sane defaults
zinit $load willghatch/zsh-saneopt
# directory autojump
zinit $load rupa/z

# ls colors
zinit ice nocompile:! pick:c.zsh atpull:%atclone atclone:'dircolors -b LS_COLORS > c.zsh'
zinit $load trapd00r/LS_COLORS

# zsh autosuggestions
zinit ice silent wait:1 atload:_zsh_autosuggest_start
zinit $load zsh-users/zsh-autosuggestions

# zsh extra completions
zinit ice blockf; zinit $load zsh-users/zsh-completions

# better directory listing
zinit ice silent wait:1; zinit $load supercrabtree/k

# auto-ls; self-explanatory
zinit ice wait'0' lucid
zinit $load desyncr/auto-ls

# direnv integration
zinit $load ptavares/zsh-direnv

# notifies when a long-running job has completed using `notify-send`
zinit $load MichaelAquilina/zsh-auto-notify

# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh

