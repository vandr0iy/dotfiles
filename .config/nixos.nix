# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

{
  imports =
    [
      ./hardware-configuration.nix
    ];

  boot = { 
    initrd.luks.devices = [
      {
        name = "root";
        device = "/dev/sda2";
        preLVM = true;
      }
    ];
    loader.grub = {
      efiSupport = true;
      device = "nodev";
    };
  };

  networking = {
    hostName = "fingolfin";
    usePredictableInterfaceNames = false; # Because fuck that shit
    networkmanager.enable = true;
  };

  # Select internationalisation properties.
  i18n = {
    consoleFont = "Lat2-Terminus16";
    consoleKeyMap = "us";
    defaultLocale = "en_US.UTF-8";
  };

  time.timeZone = "Europe/Zurich";

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with pkgs; [
    # basics 
    exfat
    exfat-utils
    file
    git
    htop
    killall
    unzip
    vim
    wget 
    which
    zsh

    # programming
    python3
    mysql57

    # multimedia
    mpv
    python37Packages.youtube-dl
    mplayer
    ffmpeg_4

    # network
    networkmanager

    # security
    oathToolkit

    # nix utils
    nix-prefetch-scripts
    
    # graphical applications
    compton
    firefox
    gparted
    i3-gaps
    i3status-rust
    icecat
    kate
    libreoffice
    lxqt.pavucontrol-qt
    plasma-nm
    plasma-pa
    tdesktop
    thunderbird
    xscreensaver

    # muh gaymes
    steam

    # custom packages
    #( import ./from-source/oathtool.nix)
  ];

  nixpkgs.config = {
    allowUnfree = true;
  };

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.mtr.enable = true;
  # programs.gnupg.agent = { enable = true; enableSSHSupport = true; };

  # List services that you want to enable:

  # Enable the OpenSSH daemon.
  # services.openssh.enable = true;

  # Open ports in the firewall.
  # networking.firewall.allowedTCPPorts = [ ... ];
  # networking.firewall.allowedUDPPorts = [ ... ];
  # Or disable the firewall altogether.
  # networking.firewall.enable = false;


  # Enable sound.
  sound.enable = true;
  hardware.pulseaudio.enable = true;
  hardware.pulseaudio.support32Bit = true;

  # steam settings
  hardware.opengl.driSupport32Bit = true;

  services = {
    # Enable the X11 windowing system.
    xserver = {
      enable = true;
      layout = "us";
      # services.xserver.xkbOptions = "eurosign:e";
  
      # Enable touchpad support.
      libinput.enable = true;
  
      # Enable the KDE Desktop Environment.
      displayManager.sddm.enable = true;
      desktopManager.plasma5.enable = true;
    };

    # Enable CUPS to print documents.
    printing.enable = true;
    mysql = {
      package = pkgs.mysql57;
      enable = true;
    };
  };

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.vandr0iy = {
    isNormalUser = true;
    shell = pkgs.zsh;
    group = "users";
    createHome = true;
    extraGroups = [ "wheel" "video" "audio" "disk" "networkmanager" ]; # Enable ‘sudo’ for the user.
  };

  # This value determines the NixOS release with which your system is to be
  # compatible, in order to avoid breaking some software such as database
  # servers. You should change this only after NixOS release notes say you
  # should.
  system.stateVersion = "19.03"; # Did you read the comment?

}
