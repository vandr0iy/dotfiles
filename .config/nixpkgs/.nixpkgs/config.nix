{
  allowUnfree = true;

  chromium = {
    enablePepperFlash = true;
  };

  polybar = pkgs.polybar.override {
    i3Support = true;
  };

  packageOverrides = pkgs: with pkgs; {
    world = pkgs.buildEnv {
      name = "world";

      paths = [
        # mendeley
        # thunderbird-bin
        # browsers
        tor-browser-bundle-bin
        chromium

        # editors
        emacs
        vim_configurable

        # network tools
        bind

        # stuff for work
        ansible_2_7
        dbeaver-ce
        idea-community
        leiningen
        mysql
        nodejs-slim
        openjdk
        otpw
        slack

        # programming languages
        go
        go-mtpfs        # transfer files to android phone: go-mtpfs ~/mnt, fusermount -u ~/mnt

        # ricer shenanigans
        feh
        flameshot
        numix-solarized-gtk-theme
        polybar
        ranger
        rofi-unwrapped
        xsel-unstable
        yadm

        # fonts
        font-awesome
        material-icons
        terminus-font
        terminus-font-ttf

        # utils
        gnupg
        gpgme
        graphviz
        haskellPackages.pandoc
        imagemagick
        lm-sensors
        ncdu
        neofetch
        poppler_utils   # pdf library and utils
        tmux
        transmission
        unetbootin      # make bootable USB keys from ISO images
        youtube-dl

        # graphical tools
        anki            # flashcards
        deluge          # bittorrent client
        libreoffice
        gnuplot_qt
        gtypist
        kcalc
        gimp
        krita
        inkscape        # vector drawing
        digikam         # photo management/viewer (needs kde themes below)

        # weechat
        # keepassx2       # qt-based password manager

        kdiff3          # diff/merge tool
        xarchiver       # simple UI to browse archives
        httrack         # website downloader

        # deadbeef-with-plugins  # music player
            # always check hardware is receiving 44.1Khz and no resampling
            # is happening!
            # use "pactl info" and e.g. cat /proc/asound/card0/pcm0p/sub0/hw_params
            # plugins inside ~/.local/lib/deadbeef

        # media
        audacity               # audio editor
        vlc             # plays anything
        mpv             # good hardware video decoding
        smplayer        # richer UI for mpv
        calibre         # ebook viewer

        # muh gaemz
        steam           # needed for Civ6

      ];
      pathsToLink = [ "/share" "/bin" ];
    };
  };
}
