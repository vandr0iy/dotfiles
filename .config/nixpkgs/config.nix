{ pkgs }: {


  packageOverrides = super: let self = super.pkgs; in with self;
  rec {

    polybar-i3 = pkgs.polybar.override {
      i3Support = true;
    };
    programs = {
      adb.enable = true;
      gnupg.agent.enable = true;
      gnupg.agent.pinentryFlavor = "qt";
    };

    environment.extraInit = with pkgs; let loader = "ld-linux-x86-64.so.2"; in ''
      export LD_LIBRARY_PATH="$LD_LIBRARY_PATH:/run/current-system/sw/lib"
      ln -fs ${pkgs.glibc}/lib/${loader} /lib/${loader}
    '';

    #vim-with-plugins = vim_configurable.customize {
    #  name = "vim-with-plugins";
    #}
    unstable = import <unstable> { inherit config; };


    world = pkgs.buildEnv {
      name = "world";
      paths = [
        #mercurial
        #unetbootin
        ack
        ag
        asciinema
        ansible
        any-nix-shell
        appimage-run
        arduino
        audacity
        autojump
        autoflake
        awscli
        bc
        bind
        blender
        cabal-install
        cabal2nix
        clojure
        dbeaver
        deluge
        dfu-util
        direnv
        emacs
        feh
        figlet
        filezilla
        fish
        flameshot
        flashrom
        font-awesome_5
        freecad
        fzf
        gcc
        ghostwriter
        gimp
        git-lfs
        glxinfo
        gnumake
        gnupg
        go
        google-chrome
        gparted
        gpgme
        haskellPackages.greenclip
        i3-easyfocus
        ipcalc
        jetbrains.idea-community
        jq
        kcalc
        kdeApplications.krdc
        kitty
        lastpass-cli
        leiningen
        libnotify
        librecad2
        lolcat
        lm_sensors
        lxappearance
        material-icons
        minicom
        mongodb
        mongodb-compass
        mongodb-tools
        mosh
        mpd
        mysql
        ncmpcpp
        neofetch
        nerdfonts
        networkmanagerapplet
        nix-prefetch-git
        nnn
        nmap_graphical
        nodePackages.typescript
        nodejs
        numix-solarized-gtk-theme
        openjdk
        openscad
        openssl
        otpw
        peco
        pinentry-qt
        pipenv
        pipenv
        prettyping
        prusa-slicer
        polybar-i3
        powerline-fonts
        putty
        python38Full
        python38Packages.importmagic
        python38Packages.isort
        python38Packages.jsbeautifier
        python38Packages.pillow
        python38Packages.pip
        python38Packages.virtualenv
        python38Packages.virtualenvwrapper
        python38Packages.yapf
        pv
        qdirstat
        robomongo
        ranger
        rofi
        ruby
        screenkey
        shellcheck
        sox
        stalonetray
        starship
        stdenv
        subversion
        tdesktop
        teensy-loader-cli
        terminus_font
        #terminus-nerdfont
        testssl
        texlive.combined.scheme-full
        thefuck
        tig
        tmux
        tortoisehg
        transmission
        usbutils
        vimHugeX
        w3m
        wmctrl
        xdotool
        xsel
        yad
        yadm
        yarn
        yarn2nix
        youtube-dl
        zlib
        zlib.dev

        cura
        # stuff from unstable
        unstable.picom
        unstable.qcad
        unstable.slack
        unstable.super-slicer
        unstable.ungoogled-chromium
      ];
      pathsToLink = [ "/share" "/bin" ];
    };
  };

  oraclejdk.accept_license = true;
  android_sdk.accept_license = true;
  allowUnfree = true;
  allowBroken = false;


  #chromium = {
    #enablePepperFlash = true;
    #ungoogled = true;
  #};
}
