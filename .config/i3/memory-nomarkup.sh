#!/bin/bash
printf "RAM %3.0f/%3.0f Mb" $(free | grep Mem | awk '{print $3/1024" "$2/1024}')
