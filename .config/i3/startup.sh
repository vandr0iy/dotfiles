#!/bin/bash
#this is a startup script for i3wm

#starting all the necessary polybars
(polybar top &)
(polybar bottom &)

dp1=$(xrandr -q | grep "^DP1\ connected")
if [ "$dp1" != "" ] 
then (polybar bigmonitor &)
fi 

#setting up the background image
feh --bg-scale /home/vandr0iy/Pictures/in_gnu_we_trust.png

#updating the xrdb
xrdb .Xresources

#starting up rofi
rofi

