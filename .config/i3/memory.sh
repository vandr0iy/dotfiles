#!/bin/bash
printf "<span background=\"#002b36\" foreground=\"#93a1a1\"> RAM %3.0f/%3.0f Mb </span>" $(free | grep Mem | awk '{print $3/1024" "$2/1024}')
