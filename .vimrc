set nocompatible              " be iMproved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
"Plugin 'godlygeek/tabular'
Plugin 'Chiel92/vim-autoformat'
Plugin 'Shougo/deoplete.nvim'
Plugin 'VundleVim/Vundle.vim'
Plugin 'Yggdroot/indentLine'
Plugin 'airblade/vim-gitgutter'
Plugin 'bling/vim-bufferline'
Plugin 'chrisbra/Colorizer'
Plugin 'ctrlpvim/ctrlp.vim'
Plugin 'elzr/vim-json'
Plugin 'ervandew/supertab'
Plugin 'kburdett/vim-nuuid'
Plugin 'liuchengxu/graphviz.vim'
Plugin 'nathanaelkane/vim-indent-guides'
Plugin 'romainl/flattened'
Plugin 'rosstimson/bats.vim'
Plugin 'scrooloose/nerdcommenter'
Plugin 'scrooloose/nerdtree'
Plugin 'tomtom/tlib_vim'
Plugin 'tpope/vim-surround'
Plugin 'tpope/vim-fugitive'
Plugin 'vim-airline/vim-airline'
Plugin 'vim-airline/vim-airline-themes'
Plugin 'vim-latex/vim-latex'
Plugin 'vim-scripts/Solarized'
Plugin 'vim-syntastic/syntastic'
" clojure stuff
Plugin 'tpope/vim-fireplace'
Plugin 'tpope/vim-salve'
Plugin 'guns/vim-clojure-static'
Plugin 'guns/vim-clojure-highlight'
Plugin 'kien/rainbow_parentheses.vim'

" The following are examples of different formats supported.
" Keep Plugin commands between vundle#begin/end.

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required
" To ignore plugin indent changes, instead use:
"filetype plugin on
"
" Brief help
" :PluginList       - lists configured plugins
" :PluginInstall    - installs plugins; append `!` to update or just :PluginUpdate
" :PluginSearch foo - searches for foo; append `!` to refresh local cache
" :PluginClean      - confirms removal of unused plugins; append `!` to auto-approve removal
"
" see :h vundle for more details or wiki for FAQ
" Put your non-Plugin stuff after this line
set number
set encoding=utf-8
set fileencodings=utf-8
highlight LineNr term=bold cterm=NONE ctermfg=DarkGreen ctermbg=NONE gui=NONE guifg=DarkGreen guibg=NONE
syntax on
syntax enable

" fix vim flashing
set noerrorbells visualbell t_vb=
if has('autocmd')
  autocmd GUIEnter * set visualbell t_vb=
endif

" Airline setup
autocmd FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o

filetype plugin on
set grepprg=grep\ -nH\ $*
let g:tex_flavor='latex'

set colorcolumn=100
set cursorline
" set Vim-specific sequences for RGB colors
let &t_8f = "\<Esc>[38;2;%lu;%lu;%lum"
let &t_8b = "\<Esc>[48;2;%lu;%lu;%lum"

set t_Co=256
set termguicolors
let g:solarized_termcolors=16
"  colorscheme solarized
colorscheme flattened_dark
set background=dark
set guifont=xos4\ Terminus\ 12
set guioptions-=T " Removes top toolbar
set guioptions-=r " Removes right hand scroll bar
set go-=L " Removes left hand scroll bar
set linespace=15
set mouse=a

set showmode                    " always show what mode we're currently editing in
set nowrap                      " don't wrap lines
set tabstop=2                   " a tab is four spaces
set smarttab
set tags=tags
set softtabstop=2               " when hitting <BS>, pretend like a tab is removed, even if spaces
set expandtab                   " expand tabs by default (overloadable per file type later)
set shiftwidth=2                " number of spaces to use for autoindenting
set shiftround                  " use multiple of shiftwidth when indenting with '<' and '>'
set backspace=indent,eol,start  " allow backspacing over everything in insert mode
set autoindent                  " always set autoindenting on
set copyindent                  " copy the previous indentation on autoindenting
set rnu
set nu
set ignorecase                  " ignore case when searching
set smartcase                   " ignore case if search pattern is all lowercase,
set timeout timeoutlen=200 ttimeoutlen=100
set visualbell           " don't beep
set noerrorbells         " don't beep
set autowrite  "Save on buffer switch
set mouse=a
set laststatus=2

" show tab chars
set list
"set listchars=eol:¦,tab:␉·,trail:.,nbsp:⎵
set listchars=eol:¦,tab:>-,trail:.,nbsp:⎵
highlight NonText term=bold cterm=NONE ctermfg=12 ctermbg=NONE gui=NONE guifg=#295864 guibg=NONE
highlight Normal ctermbg=NONE guibg=NONE


" indentLine settings
"let g:indentLine_color_gui = '#184753'
let g:indentLine_color_gui = '#0F3E4A'
"let g:indentLine_color_gui = '#073642'
let g:indentLine_char = '│'

" vim_json and the thing that fucks up quotes
let g:vim_json_syntax_conceal = 0
" use this if vim_json gets uninstalled:
"autocmd InsertEnter *.json setlocal concealcursor=
"autocmd InsertLeave *.json setlocal concealcursor=inc

" airline settings
let g:airline_powerline_fonts = 1
let g:airline_theme = 'solarized'

if !exists('g:airline_symbols')
  let g:airline_symbols = {}
endif

" unicode symbols
let g:airline_left_sep = '»'
let g:airline_left_sep = '▶'
let g:airline_right_sep = '«'
let g:airline_right_sep = '◀'
let g:airline_symbols.linenr = '␊'
let g:airline_symbols.linenr = '␤'
let g:airline_symbols.linenr = '¶'
let g:airline_symbols.branch = '⎇'
let g:airline_symbols.paste = 'ρ'
let g:airline_symbols.paste = 'Þ'
let g:airline_symbols.paste = '∥'
let g:airline_symbols.whitespace = 'Ξ'

" airline symbols
let g:airline_left_sep = ''
let g:airline_left_alt_sep = ''
let g:airline_right_sep = ''
let g:airline_right_alt_sep = ''
let g:airline_symbols.branch = ''
let g:airline_symbols.readonly = ''
let g:airline_symbols.linenr = ''

" With a map leader it's possible to do extra key combinations
" like <leader>w saves the current file
let mapleader = ","
let g:mapleader = ","

" Down is really the next line
nnoremap j gj
nnoremap k gk

"Auto change directory to match current file ,cd
nnoremap ,cd :cd %:p:h<CR>:pwd<CR>

"easier window navigation

nmap <C-n> <C-w>h
nmap <C-e> <C-w>j
nmap <C-i> <C-w>k
nmap <C-o> <C-w>l

" Resize vsplit
nmap <C-y> :vertical resize +5<cr>
nmap <C-l> :vertical resize -5<cr>
" Tab around for the buffers
nnoremap <C-S-tab> :bprevious<cr>
nnoremap <C-tab>   :bnext<cr>
nmap <C-b> :NERDTreeToggle<cr>

" Create split below
nmap :sp :rightbelow sp<cr>
" set folding method
set fdm=indent
set foldlevelstart=20
autocmd Syntax c,cpp,vim,xml,html,xhtml,json setlocal foldmethod=syntax
autocmd Syntax c,cpp,vim,xml,html,xhtml,perl normal zR

" Quickly go forward or backward to buffer
nmap :bp :BufSurfBack<cr>
nmap :bn :BufSurfForward<cr>

" highlight Search cterm=underline

" Swap files out of the project root
set backupdir=~/.vim/backup//
set directory=~/.vim/swap//

" Easy motion stuff
let g:EasyMotion_leader_key = '<Leader>'

autocmd cursorhold * set nohlsearch
autocmd cursormoved * set hlsearch

" IMPORTANT: grep will sometimes skip displaying the file name if you
" search in a singe file. This will confuse Latex-Suite. Set your grep
" program to always generate a file-name.
set grepprg=grep\ -nH\ $*

" haskell settings
set completeopt=menuone,menu,longest

set wildignore+=*\\tmp\\*,*.swp,*.swo,*.zip,.git,.cabal-sandbox
set wildmode=longest,list,full
set wildmenu
set completeopt+=longest

" Syntastic defaults
map <Leader>s :SyntasticToggleMode<CR>

set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 0
let g:syntastic_check_on_open = 0
let g:syntastic_check_on_wq = 0

com! FormatJSON %! jq .
com! Pasteclip :r !xsel -b
autocmd FileType javascript setlocal equalprg=js-beautify\ --stdin

nmap <C-S-j> :FormatJSON<cr>
nmap <C-S-p> :Pasteclip<cr>

" rainbow parentheses always on
au VimEnter * RainbowParenthesesToggle
au Syntax * RainbowParenthesesLoadRound
au Syntax * RainbowParenthesesLoadSquare
au Syntax * RainbowParenthesesLoadBraces

" markdown preview; this opens the current file in chromium.
" NOTE: must have the markdown preview extension installed.
command MarkdownPreview !(chromium % > /dev/null 2>&1 &)
map <silent><C-t> :MarkdownPreview<cr><cr>


""" Graphviz options
let g:graphviz_output_format = 'png'

""" REMAPPINGS FOR COLEMAK """
""" !!! EXPERIMENTAL !!! """
" Require Vim >=7.0 {{{
    if v:version < 700 | echohl WarningMsg | echo "You need Vim version 7.0 or later." | echohl None | finish | endif
" }}}
" Up/down/left/right {{{
    nnoremap n h|xnoremap n h|onoremap n h|
    nnoremap e j|xnoremap e j|onoremap e j|
    nnoremap i k|xnoremap i k|onoremap i k|
    nnoremap o l|xnoremap o l|onoremap o l|

    nnoremap N H|xnoremap N H|onoremap N H|
    nnoremap E J|xnoremap E J|onoremap E J|
    nnoremap I K|xnoremap I K|onoremap I K|
    nnoremap O L|xnoremap O L|onoremap O L|

    nnoremap h n|xnoremap h n|onoremap h n|
    nnoremap j e|xnoremap j e|onoremap j e|
    nnoremap k i|xnoremap k i|onoremap k i|
    nnoremap l o|xnoremap l o|onoremap l o|

    nnoremap H N|xnoremap H N|onoremap H N|
    nnoremap J E|xnoremap J E|onoremap J E|
    nnoremap K I|xnoremap K I|onoremap K I|
    nnoremap L O|xnoremap L O|onoremap L O|
" }}}
" Window handling {{{
    nnoremap <C-W>n <C-W>h|xnoremap <C-W>n <C-W>h|
    nnoremap <C-W>e <C-W>j|xnoremap <C-W>e <C-W>j|
    nnoremap <C-W>i <C-W>k|xnoremap <C-W>i <C-W>k|
    nnoremap <C-W>o <C-W>l|xnoremap <C-W>o <C-W>l|
" }}}
" Folds, etc. {{{
    nnoremap zn zj|xnoremap zn zj|
    nnoremap ze zk|xnoremap ze zk|
" }}}
